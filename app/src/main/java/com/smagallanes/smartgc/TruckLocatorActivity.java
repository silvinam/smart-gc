package com.smagallanes.smartgc;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.snackbar.Snackbar;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.android.data.kml.KmlContainer;
import com.google.maps.android.data.kml.KmlLayer;
import com.google.maps.android.data.kml.KmlLineString;
import com.google.maps.android.data.kml.KmlPlacemark;
import com.google.maps.android.data.kml.KmlPoint;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.TravelMode;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class TruckLocatorActivity extends BaseActivity implements OnMapReadyCallback {
    private static final String TAG = TruckLocatorActivity.class.getSimpleName();
    private GoogleMap mMap;

    private final static String CHANNEL_ID = "Main_channel";
    private NotificationCompat.Builder mBuilder;
    private TextView etaTextView;
    private TextView distanceTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_truck_locator);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        Button resetMap = findViewById(R.id.reset_map);
        resetMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LatLng cordoba = new LatLng(-31.3537456,-64.1721349);
                if (mMap != null) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cordoba, 16));
                }
            }
        });
        etaTextView = findViewById(R.id.eta_textview);
        distanceTextView = findViewById(R.id.distance_textview);
        initNotificationChannel();
        initNotificationBuilder();

        setTitle(getString(R.string.ubicar_camion));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng cordoba = new LatLng(-31.3537456,-64.1721349);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cordoba, 16));try {
            KmlLayer layer = new KmlLayer(mMap, R.raw.ruta_camion_basura_a_domicilio, getApplicationContext()); // creating the kml layer
            layer.addLayerToMap();// adding kml layer with the **google map**
            final Iterable<KmlContainer> containers = layer.getContainers();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final ArrayList<LatLng> truckPositionSimulation = new ArrayList<>();
                    final ArrayList<LatLng> routeWaypoints = new ArrayList<>();
                    ArrayList<LatLng> pinLocations = new ArrayList<>();
                    for (KmlContainer container : containers) {
//                        Log.i(TAG, "Iterating over container hasContainers: " + container.hasContainers() + " has placemarks: " + container.hasPlacemarks());
                        boolean isSimulationFilled = false;
                        for (KmlContainer containerHijo : container.getContainers()) {
//                            Log.i(TAG, "Iterating over child container: " + containerHijo.hasContainers() + " has placemarks: " + containerHijo.hasPlacemarks());
                            for (KmlPlacemark placemark : containerHijo.getPlacemarks()) {
//                                Log.i(TAG, "Iterating over palcemarks: " + placemark.getGeometry().getGeometryType());
                                if(placemark.getGeometry().getGeometryType().equals("Point")) {
                                    KmlPoint point = (KmlPoint) placemark.getGeometry();
                                    LatLng latLng = new LatLng(point.getGeometryObject().latitude, point.getGeometryObject().longitude);
                                    pinLocations.add(latLng);
                                    Log.i(TAG, "Point latlng: " + latLng);
                                }
                                if (placemark.getGeometry().getGeometryType().equals("LineString")) {
                                    KmlLineString lineString = (KmlLineString) placemark.getGeometry();
//                                    Log.i(TAG, "lineString: " + lineString.getGeometryType());

                                    if (!isSimulationFilled) {
                                        truckPositionSimulation.addAll(lineString.getGeometryObject());
                                        isSimulationFilled = true;
                                    } else {
                                        routeWaypoints.addAll(lineString.getGeometryObject());
                                    }
                                }
                            }
                        }
                    }

                    final LatLng startLocation = new LatLng(-31.3540047,-64.1757707);
                    final LatLng houseLocation = new LatLng(-31.3521977,-64.1704647);
                    final LatLng endLocation = new LatLng(-31.3525757,-64.1686124);
                    for (int i = 0; i < pinLocations.size(); i++) {
                        BitmapDescriptor descriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE);
                        String name = "PIN";
                        if (pinLocations.get(i).equals(startLocation)) {
                            descriptor = BitmapDescriptorFactory.fromBitmap(resizeMapIcons("inicio", 150, 150));
                            name = "Inicio ruta";
                        }
                        if (pinLocations.get(i).equals(houseLocation)) {
                            descriptor = BitmapDescriptorFactory.fromBitmap(resizeMapIcons("house", 80, 80));
                            name = "Ubicación domicilio";
                        }
                        if (pinLocations.get(i).equals(endLocation)) {
                            descriptor = BitmapDescriptorFactory.fromBitmap(resizeMapIcons("fin", 150, 150));
                            name = "Fin ruta";
                        }
                        final BitmapDescriptor finalDescriptor = descriptor;
                        final MarkerOptions options = new MarkerOptions().position(pinLocations.get(i)).title(name)
                                .icon(finalDescriptor);
                        TruckLocatorActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mMap.addMarker(options);
                            }
                        });
                    }
                    BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(resizeMapIcons("camion_basura", 100, 100));
                    final Marker[] truckMarker = new Marker[1];
                    long elapsedTime = System.currentTimeMillis();
                    boolean firstTime = true;
                    final boolean[] isNotified = {false};
                    for (final LatLng currTruckPos : truckPositionSimulation) {
//                        Log.i(TAG, "LineString latlng: " + latLng);
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        TruckLocatorActivity.this.runOnUiThread(() -> {
                            if (truckMarker[0] != null) {
                                truckMarker[0].remove();
                            }
                            Marker marker = mMap.addMarker(new MarkerOptions().position(currTruckPos)
                                    .icon(icon));
                            truckMarker[0] = marker;
                        });
                        ArrayList<LatLng> copyOfRouteWaypoints = new ArrayList<>();
                        for (LatLng latLng : routeWaypoints) {

                            float[] results = new float[1];
                            Location.distanceBetween(latLng.latitude,
                                    latLng.longitude,
                                    currTruckPos.latitude,
                                    currTruckPos.longitude, results);
                            if (results[0] > 70) {
                                copyOfRouteWaypoints.add(latLng);
                            } else {
//                                Log.i(TAG, ">>>>> Distance: " + results[0]);
                            }
                        }
                        routeWaypoints.clear();
                        routeWaypoints.addAll(copyOfRouteWaypoints);
                        if (firstTime || System.currentTimeMillis() - elapsedTime > 5000) {
                            firstTime = false;
                            elapsedTime = System.currentTimeMillis();
                            Log.i(TAG, "routeWaypoints size: " + routeWaypoints.size());
                            Collection<List<LatLng>> partitionedList = partitionBasedOnSize(routeWaypoints, 23);
                            Log.i(TAG, "partitionedList size: " + partitionedList.size());

                            long totalDuration = 0;
                            long totalDistance = 0;

                            for (List<LatLng> smallList : partitionedList) {
                                Log.i(TAG, "Small list size: " + smallList.size());
                                com.google.maps.model.LatLng[] waypoints = new com.google.maps.model.LatLng[smallList.size()];
                                for (int k = 0; k < smallList.size(); k++) {
                                    LatLng latLngFromSmallList = smallList.get(k);
                                    waypoints[k] = new com.google.maps.model.LatLng(latLngFromSmallList.latitude, latLngFromSmallList.longitude);
                                }
                                Log.i(TAG, ">>>>>>>>>>>>>>>>> About to perform ETA request");
                                try {
                                    com.google.maps.model.LatLng startPos = new com.google.maps.model.LatLng(currTruckPos.latitude, currTruckPos.longitude);
                                    com.google.maps.model.LatLng endPos = new com.google.maps.model.LatLng(houseLocation.latitude, houseLocation.longitude);
                                    DirectionsResult result = DirectionsApi.newRequest(getGeoContext())
                                            .waypoints(waypoints)
                                            .mode(TravelMode.DRIVING)
                                            .origin(startPos)
                                            .destination(endPos)
                                            .departureTime(Instant.now())
                                            .await();
                                    if (result.routes.length > 0) {
                                        int legsSize = result.routes[0].legs.length;
                                        Log.i(TAG, ">>>>>>>>>>>>>>>>> result.routes[0].legs.length: " + legsSize);
                                        for (DirectionsLeg leg : result.routes[0].legs) {
                                            totalDuration += leg.duration.inSeconds;
                                            totalDistance += leg.distance.inMeters;
                                        }
                                        Log.i(TAG," totalDuration : " + totalDuration + " totalDistance: " + totalDistance);
                                    } else {
                                        Log.e(TAG, "Result from direction api is empty");
                                    }
                                } catch (ApiException | InterruptedException | IOException e) {
                                    Log.e(TAG, "Error al llamar DirectionsApi newRequest await: " + e.getMessage());
                                }
                            }
                            Log.i(TAG, ">>>>>>>>>>>>>>>>> ETA: " + totalDuration + " segundos");
                            long finalTotalDuration = totalDuration;
                            long finalTotalDistance = totalDistance;
                            TruckLocatorActivity.this.runOnUiThread(() -> {
                                String timeToSet;
                                if (finalTotalDuration > 60) {
                                    timeToSet = (finalTotalDuration/60) + " minutos";
                                } else {
                                    timeToSet = finalTotalDuration + " segundos";
                                }
                                etaTextView.setText(timeToSet);
                                distanceTextView.setText(finalTotalDistance + " metros");
                                if (finalTotalDuration < 60*5) { // 5 minutos
                                    //Toast.makeText(TruckLocatorActivity.this, "CAMION DE BASURA PROXIMO AL DOMICILIO!", Toast.LENGTH_LONG).show();
                                    if (!isNotified[0]) {
                                        Snackbar.make(findViewById(android.R.id.content), "CAMION DE BASURA PROXIMO AL DOMICILIO!", Snackbar.LENGTH_SHORT).show();
                                        if (areNotificationsEnabled()) {
                                            sendNotificationToChannel(getString(R.string.truck_near_title), getString(R.string.truck_near_body));
                                            isNotified[0] = true;
                                        } else {
                                            Log.i(TAG, "Notifications are not enabled.");
                                        }
                                    } else {
                                        Log.d(TAG, "Ya se notificó");
                                    }
                                }
                            });
                        } else {
//                            Log.i(TAG, "5 seconds need to pass before requesting ETA again");
                        }
                    }
                }
            }).start();
        } catch (Exception e) {
            Log.e(TAG, "Error al agregar layer de recorrido de camión");
        }
    }

    public Bitmap resizeMapIcons(String iconName, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),getResources().getIdentifier(iconName, "drawable", getPackageName()));
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    private GeoApiContext getGeoContext() {
        GeoApiContext.Builder geoApiContextBuilder = new GeoApiContext.Builder();
        return geoApiContextBuilder.queryRateLimit(3)
                .apiKey(getString(R.string.directionsApiKey))
                .connectTimeout(1, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.SECONDS)
                .writeTimeout(1, TimeUnit.SECONDS).build();
    }

    static <T> Collection<List<T>> partitionBasedOnSize(List<T> inputList, int size) {
        final AtomicInteger counter = new AtomicInteger(0);
        return inputList.stream()
                .collect(Collectors.groupingBy(s -> counter.getAndIncrement()/size))
                .values();
    }

    private void sendNotificationToChannel(String notificationTitle, String notificationBody) {
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(getApplicationContext());
        Log.d(TAG, "Mostrando notificacion");

        mBuilder.setContentText(notificationTitle) //"El camion de basura especial pasará pronto por su domicilio."
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(notificationBody))
                .setSmallIcon(R.drawable.ic_launcher_round)
                //.bigText("El camion de basura especial pronto pasará por su domicilio. Controle que la basura esté afuera."))
                .setTicker("Aca va algo...");

        notificationManager.notify(0, mBuilder.build());
    }

    private void initNotificationBuilder() {
        mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Nueva notificación de SMART GC")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
    }

    private void initNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}