package com.smagallanes.smartgc;

import android.os.Bundle;
import android.widget.ListView;

import com.smagallanes.smartgc.adapter.FeedListAdapter;
import com.smagallanes.smartgc.model.FeedItem;

import java.util.ArrayList;
import java.util.List;

public class NewsActivity extends BaseActivity {

    private List<FeedItem> feedItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        setTitle(getString(R.string.noticias));

        populateNews();
        ListView listView = findViewById(R.id.list);

        FeedListAdapter listAdapter = new FeedListAdapter(this, feedItems);
        listView.setAdapter(listAdapter);
    }

    private void populateNews() {
        feedItems = new ArrayList<>();
        FeedItem item1 = new FeedItem(1, "Regularizado", R.drawable.servicio_regularizado, "Servicio regularizado",
            R.drawable.regularizado, "Hoy", "Zona regularizada: Barrio Liceo 1era sección");

        FeedItem item2 = new FeedItem(2, "Atención", R.drawable.servicio_interrumpido, "Servicio interrumpido temporalmente",
            R.drawable.atencion, "19 de Marzo de 2021", "Zonas afectadas: Barrio Liceo 1era, 2da y 3era sección. Barrio Jorge Newbery. Barrio Jeronimo Luis de Cabrera.");

        FeedItem item3 = new FeedItem(3, "Info", R.drawable.camio_horario, "Cambio de horario",
                R.drawable.info, "15 de Marzo de 2021", "Zonas afectadas: Barrio Alta Cordoba.");

        feedItems.add(item1);
        feedItems.add(item2);
        feedItems.add(item3);
    }
}