package com.smagallanes.smartgc;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.functions.FirebaseFunctionsException;
import com.smagallanes.smartgc.functions.CallFunctionHelper;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class SpecialServiceRequestActivity extends BaseActivity {
    private static final String TAG = SpecialServiceRequestActivity.class.getSimpleName();
    private static final String SCHEDULED_SERVICES = "SCHEDULED_SERVICES";

    // Obtener referencia al EditText
    private TextView textViewDate;

    private RadioButton radioButton1;

    private View requestServiceButton;
    private View clearServiceButton;
    private TextView scheduledServicesTextView;

    private final String[] rangesOption1 = {"14", "22hs"};
    private final String[] rangesOption2 = {"6", "14hs"};

    private String[] rangeToUse;

    // Guardar el último año, mes y día del mes
    private int chosenYear, chosenMonth, chosenDayOfMonth;

    // Crear un listener del datepicker;
    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            // Esto se llama cuando seleccionan una fecha. Nos pasa la vista, pero más importante, nos pasa:
            // El año, el mes y el día del mes. Es lo que necesitamos para saber la fecha completa


            // Refrescamos las globales
            chosenYear = year;
            chosenMonth = monthOfYear;
            chosenDayOfMonth = dayOfMonth;

            // Y refrescamos la fecha
            refreshDateInEditText();

            requestServiceButton.setEnabled(true);
        }
    };

    public void refreshDateInEditText() {
        // Formateamos la fecha pero podríamos hacer cualquier otra cosa ;)
        String date = String.format(Locale.getDefault(), "%02d-%02d-%02d", chosenYear, chosenMonth+1, chosenDayOfMonth);
        String rango = "";
        if (rangeToUse.length > 1) {
            rango = getString(R.string.rango_horario, rangeToUse[0], rangeToUse[1]);
        }
        // La ponemos en el editText
        textViewDate.setText(date + " " + rango);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special_service_request);

        // Instanciar objetos
        textViewDate = findViewById(R.id.etDate);

        requestServiceButton = findViewById(R.id.request_service_button);
        clearServiceButton = findViewById(R.id.clear_service_button);
        scheduledServicesTextView = findViewById(R.id.servicios_agendados);

        radioButton1 = findViewById(R.id.radioButton);

        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            requestServiceButton.setEnabled(false);
            textViewDate.setText(R.string.please_select_date);
        });

        // Poner último año, mes y día a la fecha de hoy
        final Calendar calendar = Calendar.getInstance();
        chosenYear = calendar.get(Calendar.YEAR);
        chosenMonth = calendar.get(Calendar.MONTH) ;
        chosenDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        // Refrescar la fecha en el EditText
        //refreshDateInEditText();
        textViewDate.setText(R.string.please_select_date);

        // Hacer que el datepicker se muestre cuando toquen el EditText; recuerda
        // que se podría invocar en el click de cualquier otro botón, o en cualquier
        // otro evento

        textViewDate.setOnClickListener(v -> {
            // Aquí es cuando dan click así que mostramos el DatePicker

            // Le pasamos lo que haya en las globales
            DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(dateSetListener, chosenYear, chosenMonth, chosenDayOfMonth);

            // Setting Min Date to today date
            Calendar minDate = Calendar.getInstance();
            datePickerDialog.setMinDate(minDate);

            //// Fecha maxima 2 años
            Calendar maxDate = Calendar.getInstance();
            maxDate.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR) + 2);
            datePickerDialog.setMaxDate(maxDate);
            HashSet<Integer> daysToDisable = new HashSet<>();
            boolean isEveryTwoWeeks = false;
            if (radioButton1.isChecked()) {
                daysToDisable.add(Calendar.MONDAY);
                daysToDisable.add(Calendar.TUESDAY);
                daysToDisable.add(Calendar.WEDNESDAY);
                daysToDisable.add(Calendar.FRIDAY);
                daysToDisable.add(Calendar.SATURDAY);
                daysToDisable.add(Calendar.SUNDAY);
                rangeToUse = rangesOption1;
            } else {
                isEveryTwoWeeks = true;
                daysToDisable.add(Calendar.MONDAY);
                daysToDisable.add(Calendar.WEDNESDAY);
                daysToDisable.add(Calendar.THURSDAY);
                daysToDisable.add(Calendar.FRIDAY);
                daysToDisable.add(Calendar.SATURDAY);
                daysToDisable.add(Calendar.SUNDAY);
                rangeToUse = rangesOption2;
            }
            while (minDate.compareTo(maxDate) < 0) {
                int dayOfWeek = minDate.get(Calendar.DAY_OF_WEEK);
                if (isEveryTwoWeeks && minDate.get(Calendar.WEEK_OF_MONTH) % 2 == 0
                        || daysToDisable.contains(dayOfWeek)) {
                    Calendar[] disabledDays = new Calendar[1];
                    disabledDays[0] = minDate;
                    datePickerDialog.setDisabledDays(disabledDays);
                }
                minDate.add(Calendar.DAY_OF_WEEK, 1);
            }
            //Mostrar
            datePickerDialog.show(SpecialServiceRequestActivity.this.getFragmentManager(), "DatePickerDialog");
        });

        setTitle(getString(R.string.servicio_especial));

        SharedPreferences sharedPref = getSharedPreferences("SMARTGC", Context.MODE_PRIVATE);
        Set<String> scheduledServices = sharedPref.getStringSet(SCHEDULED_SERVICES, new HashSet<>());
        if (scheduledServices != null && !scheduledServices.isEmpty()) {
            String lastScheduled = scheduledServices.iterator().next();
            scheduledServicesTextView.setText(lastScheduled);
            scheduledServicesTextView.setVisibility(View.VISIBLE);
            clearServiceButton.setVisibility(View.VISIBLE);
        }
    }

    public void newSpecialService(View view) {
        String currentDateText = textViewDate.getText().toString();
        if (currentDateText.equals(getString(R.string.please_select_date))) {
            //Toast.makeText(getApplicationContext(), R.string.date_is_not_set, Toast.LENGTH_LONG).show();
            Snackbar.make(findViewById(android.R.id.content), R.string.date_is_not_set, Snackbar.LENGTH_LONG).show();
        } else {
            SharedPreferences sharedPref = getSharedPreferences("SMARTGC", Context.MODE_PRIVATE);
            String currentToken = sharedPref.getString(ReferenceAddressActivity.TOKEN_KEY, "");
            requestSpecialService(currentToken);
        }
    }

    private void requestSpecialService(final String token) {
        int currentDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH); // solo para pruebas usamos el dia de hoy
        int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int currentMinute = Calendar.getInstance().get(Calendar.MINUTE);
        int currentSecond = Calendar.getInstance().get(Calendar.SECOND);
        if (currentSecond + 20 > 60) {
            currentSecond = currentSecond + 20 - 60;
            currentMinute ++;
        } else {
            currentSecond += 20;
        }
        Task<String> requestTask = CallFunctionHelper.requestSpecialServiceRequestWithDate(token,
                chosenYear, chosenMonth, currentDay, currentHour, currentMinute, currentSecond);
        requestTask.addOnCompleteListener(task -> {
            try {
                String result = task.getResult();
                Log.i(TAG, "Result of request call: " + result);
                if (task.isSuccessful()) {
                    //Toast.makeText(getApplicationContext(), R.string.success_special_service_request, Toast.LENGTH_LONG).show();
                    Snackbar.make(findViewById(android.R.id.content), R.string.success_special_service_request, Snackbar.LENGTH_LONG).show();
                    addSpecialServiceToSet(chosenYear, chosenMonth, chosenDayOfMonth);
                    clearServiceButton.setVisibility(View.VISIBLE);
                } else {
                    Exception e = task.getException();
                    if (e instanceof FirebaseFunctionsException) {
                        FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                        FirebaseFunctionsException.Code code = ffe.getCode();
                        Object details = ffe.getDetails();
                        Log.e(TAG, "Task not successful-- code" + code + " details: " + details);
                    }
                    Toast.makeText(getApplicationContext(), R.string.error_special_service_request, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception happened when trying to get result from task. Check the cloud function." + e.getMessage());
            }
        });
    }

    public void clearSpecialServices(View view) {
        SharedPreferences sharedPref = getSharedPreferences("SMARTGC", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putStringSet(SCHEDULED_SERVICES, new HashSet<>());
        editor.apply();
        clearServiceButton.setVisibility(View.GONE);
        scheduledServicesTextView.setVisibility(View.GONE);
        Snackbar.make(findViewById(android.R.id.content), R.string.services_cleared, Snackbar.LENGTH_LONG).show();
    }

    private void addSpecialServiceToSet(int chosenYear, int chosenMonth, int chosenDayOfMonth) {
        HashSet<String> scheduledServices = new HashSet<>();
        String rango = "";
        if (rangeToUse.length > 1) {
            rango = getString(R.string.rango_horario_reminder, rangeToUse[0], rangeToUse[1]);
        }
        StringBuilder scheduledText = new StringBuilder("Tiene un servicio agendado para la fecha ");
        scheduledText.append(chosenDayOfMonth)
                .append("-")
                .append(chosenMonth)
                .append("-")
                .append(chosenYear);
        if (!rango.isEmpty()) {
            scheduledText.append(" en el rango horario ").append(rango);
        }
        scheduledServices.add(scheduledText.toString());
        SharedPreferences sharedPref = getSharedPreferences("SMARTGC", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putStringSet(SCHEDULED_SERVICES, scheduledServices);
        editor.apply();
        scheduledServicesTextView.setText(scheduledText.toString());
        scheduledServicesTextView.setVisibility(View.VISIBLE);
    }
}