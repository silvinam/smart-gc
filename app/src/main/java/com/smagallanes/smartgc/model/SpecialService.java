package com.smagallanes.smartgc.model;

public class SpecialService {

    private String userId;
    private boolean isCompleted;

    public SpecialService() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)}
    }

    public SpecialService(String userId) {
        this.userId = userId;
        this.isCompleted = false;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }
}
