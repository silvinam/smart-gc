package com.smagallanes.smartgc.model;

public class FeedItem {
    private int id;
    private int newsImageId;
    private int iconDrawableId;
    private String name;
    private String status;

    private String when;
    private String affectedZones;

    public FeedItem() {
    }

    public FeedItem(int id, String name, int newsImage, String status,
                    int icon, String when, String affectedZones) {
        super();
        this.id = id;
        this.name = name;
        this.newsImageId = newsImage;
        this.status = status;
        this.iconDrawableId = icon;
        this.when = when;
        this.affectedZones = affectedZones;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNewsImageId() {
        return newsImageId;
    }

    public void setNewsImageId(int drawable) {
        this.newsImageId = drawable;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIconDrawableId() {
        return iconDrawableId;
    }

    public void setIconDrawableId(int iconDrawableId) {
        this.iconDrawableId = iconDrawableId;
    }

    public String getWhen() {
        return when;
    }

    public void setWhen(String when) {
        this.when = when;
    }

    public String getAffectedZones() {
        return affectedZones;
    }

    public void setAffectedZones(String affectedZones) {
        this.affectedZones = affectedZones;
    }
}
