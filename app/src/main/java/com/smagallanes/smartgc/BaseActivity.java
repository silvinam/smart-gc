package com.smagallanes.smartgc;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;

import com.google.android.material.snackbar.Snackbar;

public class BaseActivity extends AppCompatActivity {

    private static final String NOTIFICATIONS_ENABLED = "NOTIFICATIONS_ENABLED";
    private final String TAG = this.getClass().getSimpleName();
    private static boolean areNotificationsEnabled = false;
    private Switch menuSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        SharedPreferences sharedPref = getSharedPreferences("SMARTGC", Context.MODE_PRIVATE);
        areNotificationsEnabled = sharedPref.getBoolean(NOTIFICATIONS_ENABLED, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (menuSwitch != null) {
            menuSwitch.setChecked(areNotificationsEnabled);
            Log.d(TAG, "onPrepareOptionsMenu areNotificationsEnabled: " + areNotificationsEnabled);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem checkable = menu.findItem(R.id.notifications_switch);

        menuSwitch = (Switch) checkable.getActionView();
        menuSwitch.setChecked(areNotificationsEnabled);
        Log.d(TAG, "onPrepareOptionsMenu areNotificationsEnabled: " + areNotificationsEnabled);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.notifications_switch:
                areNotificationsEnabled = !areNotificationsEnabled;
                item.setChecked(areNotificationsEnabled);
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public void toggleNotifications(View view) {
        areNotificationsEnabled = !areNotificationsEnabled;
        SharedPreferences sharedPref = getSharedPreferences("SMARTGC", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(NOTIFICATIONS_ENABLED, areNotificationsEnabled);
        editor.apply();
        Log.d(TAG, "toggleNotifications to " + areNotificationsEnabled);
        int resource = areNotificationsEnabled ? R.string.notifications_enabled : R.string.notifications_disaabled;
        Snackbar.make(findViewById(android.R.id.content), resource, Snackbar.LENGTH_SHORT).show();
    }

    public static boolean areNotificationsEnabled() {
        return areNotificationsEnabled;
    }

    protected void setTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }
}