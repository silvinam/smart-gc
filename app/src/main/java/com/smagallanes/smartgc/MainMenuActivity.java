package com.smagallanes.smartgc;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

public class MainMenuActivity extends BaseActivity {

    private final static String TAG = MainMenuActivity.class.getSimpleName();
    CarouselView carouselView;
    int[] carouselImages = {R.drawable.carousel_1, R.drawable.carousel_2, R.drawable.carousel_3,
            R.drawable.carousel_4, R.drawable.carousel_5, R.drawable.carousel_6};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        String address = intent.getStringExtra(ReferenceAddressActivity.EXTRA_ADDRESS);

        SharedPreferences sharedPref = getSharedPreferences("SMARTGC", Context.MODE_PRIVATE);
        String username = sharedPref.getString(ReferenceAddressActivity.USERNAME_KEY, "");

        if (address == null || address.isEmpty()) {
            address = sharedPref.getString(ReferenceAddressActivity.ADDRESS_KEY, "Sin registrar");
        }

        // Capture the layout's TextView and set the string as its text
        TextView addressTextView = findViewById(R.id.address);
        String registeredAddress = getString(R.string.registered_address);
        addressTextView.setText(registeredAddress + " " + address);
        TextView usernameTextView = findViewById(R.id.username);
        String welcomeUser = getString(R.string.welcome_user);
        usernameTextView.setText(welcomeUser + " " + username + "!");

        carouselView = findViewById(R.id.carouselView);
        carouselView.setPageCount(carouselImages.length);
        carouselView.setImageListener(imageListener);

        setTitle(getString(R.string.main_menu));
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(carouselImages[position]);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Image clicked: " + position);
                    Intent intent = null;
                    switch(position) {
                        case 0:
                            // nada
                            break;
                        case 1:
                            // verdes
                            intent = new Intent(MainMenuActivity.this, GreenExplanationActivity.class);
                            break;
                        case 2:
                            // obra
                            intent = new Intent(MainMenuActivity.this, ConstructionWasteExplanationActivity.class);
                            break;
                        case 3:
                            // voluminosos
                            intent = new Intent(MainMenuActivity.this, BulkyExplanationActivity.class);
                            break;
                        case 4:
                            // humedos
                            intent = new Intent(MainMenuActivity.this, WetExplanationActivity.class);
                            break;
                        case 5:
                            // secos
                            intent = new Intent(MainMenuActivity.this, DryExplanationActivity.class);
                            break;
                    }
                    if (intent != null) {
                        startActivity(intent);
                    }
                }
            });
        }
    };

    /** Called when the user clicks on the "Solicitar Servicio Especial" button */
    public void requestSpecialService(View view) {
        Intent intent = new Intent(this, SpecialServiceRequestActivity.class);
        startActivity(intent);
    }

    public void openViewContainersActivity(View view) {
        Log.i(TAG, "Click Check containers");
        Intent intent = new Intent(this, ViewContainersActivity.class);
        startActivity(intent);
    }

    public void openNewsActivity(View view) {
        Log.i(TAG, "Click News");
        Intent intent = new Intent(this, NewsActivity.class);
        startActivity(intent);
    }

    public void openRecommendationsActivity(View view) {
        Log.i(TAG, "Click Recommendations");
        Intent intent = new Intent(this, RecommendationsActivity.class);
        startActivity(intent);
    }

    public void openTruckLocationActivity(View view) {
        Intent intent = new Intent(this, TruckLocatorActivity.class);
        startActivity(intent);
    }

    public void rateUs(View view) {
        Intent intent = new Intent(this, RateUsActivity.class);
        startActivity(intent);
    }
}