package com.smagallanes.smartgc;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.smagallanes.smartgc.model.User;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class ReferenceAddressActivity extends BaseActivity implements OnMapReadyCallback {
    private static final String TAG = ReferenceAddressActivity.class.getSimpleName();
    public static final String USERNAME_KEY = "USERNAME";
    public static final String ADDRESS_KEY = "ADDRESS_KEY";
    public static final String TOKEN_KEY = "TOKEN";

    private DatabaseReference mDatabaseReference;
    public static final String EXTRA_ADDRESS = "com.smagallanes.smartgc.ADDRESS";
    public static final String EXTRA_USERNAME = "com.smagallanes.smartgc.USERNAME";
    private View progressBarLayout;
    private View registrationLayout;
    private String currentToken;

    private GoogleMap mMap;
    private PlacesClient placesClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reference_address);
        progressBarLayout = findViewById(R.id.progress_bar_layout);
        registrationLayout = findViewById(R.id.registration_layout);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        ((EditText) findViewById(R.id.addressEditText)).setOnEditorActionListener(
                (v, actionId, event) -> {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                            actionId == EditorInfo.IME_ACTION_DONE ||
                            event != null &&
                                    event.getAction() == KeyEvent.ACTION_DOWN &&
                                    event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                        if (event == null || !event.isShiftPressed()) {
                            // the user is done typing.
                            Log.d(TAG, "Address to search: " + v.getText());
                            moveCameraToAddress(v.getText().toString());
                            //moveCameraToPlace(v.getText().toString());
                            return true; // consume.
                        }
                    }
                    return false; // pass on to other listeners.
                }
        );
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, 0);

        // Initialize the SDK
        Places.initialize(getApplicationContext(), getString(R.string.google_api_key));

        // Create a new PlacesClient instance
        placesClient = Places.createClient(this);
        initializeFireBase();
    }

    private void initializeFireBase() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        final FirebaseDatabase database = FirebaseDatabase.getInstance();
                        // Obtenemos la referencia de la base de datos de user-id
                        mDatabaseReference = database.getReference().child("user-id");
                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        currentToken = token;
                        writeTokenToPreference();
                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.d(TAG, msg);
                        //Toast.makeText(ReferenceAddressActivity.this, msg, Toast.LENGTH_LONG).show();
                        if (!token.isEmpty()) {
                            startTokenRegistrationFlow(token);
                        } else {
                            Log.e(TAG, "Token is empty!");
                        }
                    }
                });
        FirebaseMessaging.getInstance().subscribeToTopic("pushNotifications");
        // FirebaseMessaging.getInstance().unsubscribeFromTopic("pushNotifications") --> Options for now.
    }

    private void startTokenRegistrationFlow(final String token) {
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User foundUser = null;
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    User user = ds.getValue(User.class);
                    if (user != null) {
                        Log.d(TAG, "DeviceId: " + user.getDeviceId());
                        Log.d(TAG, "Address: " + user.getAddress());
                        // mDatabaseReference.setValue(null); // Si se descomenta esto, se borran todos los user-id
                        if (token.equals(user.getDeviceId())) {
                            foundUser = user;
                            Log.i(TAG, "Device ID already registered in the server!");
                            //Toast.makeText(ReferenceAddressActivity.this, R.string.token_registered, Toast.LENGTH_LONG).show();
                            break;
                        }
                    } else {
                        Log.e(TAG, "User is null");
                    }
                }
                if (foundUser == null) {
                    Log.i(TAG, "Habilitando UI de registro");
                    enableRegistrationUI();
                } else {
                    Log.i(TAG, "User already registered. Switching screen directly.");
                    startSpecialServiceActivity(foundUser.getAddress());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Error obteniendo dato.
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                Toast.makeText(ReferenceAddressActivity.this, "Failed to load post.",
                        Toast.LENGTH_LONG).show();
            }
        };
        // mDatabaseReference.addValueEventListener(postListener); // Para escuchar todos los eventos de cambio.
        mDatabaseReference.addListenerForSingleValueEvent(postListener); // Para escuchar el evento de cambio una sola vez.
    }

    private void enableRegistrationUI() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBarLayout.setVisibility(View.GONE);
                registrationLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    private void sendRegistrationToServer(String address) {
        User currentUser = new User(this.currentToken, address);
        // Enviar token a la referencia de la base de datos de user-id
        mDatabaseReference.push().setValue(currentUser);
    }

    /** Called when the user taps the Save button */
    public void sendAddressInfo(View view) {
        EditText addressEditText = findViewById(R.id.addressEditText);
        String address = addressEditText.getText().toString();
        EditText usernameEditText = findViewById(R.id.editTextTextPersonName2);
        String username = usernameEditText.getText().toString();
        if (address.isEmpty() || username.isEmpty()) {
            Toast.makeText(this, getString(R.string.address_username_empty), Toast.LENGTH_LONG).show();
        } else {
            Log.i(TAG, "Sending new user to server!");
            //Toast.makeText(ReferenceAddressActivity.this, R.string.sending_new_user, Toast.LENGTH_LONG).show();
            Snackbar.make(findViewById(android.R.id.content), R.string.sending_new_user, Snackbar.LENGTH_LONG).show();
            sendRegistrationToServer(address);
            writeUsernameToPreferences(username);
            startSpecialServiceActivity(address);
        }
    }

    private void startSpecialServiceActivity(String address) {
        Intent intent = new Intent(this, MainMenuActivity.class);
        intent.putExtra(EXTRA_ADDRESS, address);
        writeAddressToPreferences(address);
        startActivity(intent);
    }

    private void writeUsernameToPreferences(final String username) {
        SharedPreferences sharedPref = getSharedPreferences("SMARTGC", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(USERNAME_KEY, username);
        editor.apply();
    }

    private void writeAddressToPreferences(final String address) {
        SharedPreferences sharedPref = getSharedPreferences("SMARTGC", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(ADDRESS_KEY, address);
        editor.apply();
    }

    private void writeTokenToPreference() {
        SharedPreferences sharedPref = getSharedPreferences("SMARTGC", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(TOKEN_KEY, currentToken);
        editor.apply();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng cordoba = new LatLng(-31.3537456, -64.1721349);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cordoba, 16));

        /*Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();*/
    }

    public void moveCameraToPlace(String placeToSearch) {
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(ReferenceAddressActivity.this, R.string.permission_not_accepted, Toast.LENGTH_LONG).show();
            return;
        }
        FetchPlaceRequest fetchPlaceRequest = FetchPlaceRequest.builder(placeToSearch, Collections.singletonList(Place.Field.LAT_LNG)).build();
        Task<FetchPlaceResponse> placeResponse = placesClient.fetchPlace(fetchPlaceRequest);
        placeResponse.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                FetchPlaceResponse response = task.getResult();
                Place place = response.getPlace();
                LatLng addressLocation = place.getLatLng();
                if (addressLocation != null) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(addressLocation, 16));

                    BitmapDescriptor descriptor = BitmapDescriptorFactory.fromBitmap(resizeMapIcons("house", 80, 80));
                    MarkerOptions options = new MarkerOptions().position(addressLocation).title(placeToSearch).icon(descriptor);
                    mMap.addMarker(options);
                } else {
                    Toast.makeText(ReferenceAddressActivity.this, R.string.address_has_no_coordinates, Toast.LENGTH_LONG).show();
                }
            } else {
               Log.e(TAG, "Error obtaining place.");
                Toast.makeText(ReferenceAddressActivity.this, R.string.address_not_found, Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(e -> {
            Log.e(TAG, "Error searching for place: " + e.getMessage());
            //Something went wrong, handle the error..
        });
    }

    public void moveCameraToAddress(String strAddress){

        Geocoder coder = new Geocoder(this);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address == null || address.size() < 1) {
                address = coder.getFromLocationName(strAddress + getString(R.string.cordoba),5);
                placeMarkerToAddress(strAddress, address);
                /*Toast.makeText(ReferenceAddressActivity.this, R.string.address_not_found, Toast.LENGTH_LONG).show();
                new Thread(() -> {
                    GeoApiContext context = new GeoApiContext.Builder()
                            .apiKey(getString(R.string.google_api_key))
                            .build();
                    try {
                        GeocodingResult[] results =  GeocodingApi.geocode(context, strAddress).await();
                        if (results.length > 0) {
                            com.google.maps.model.LatLng latLng = results[0].geometry.location;
                            LatLng addressLocation = new LatLng(latLng.lat, latLng.lng);
                            ReferenceAddressActivity.this.runOnUiThread(() -> {
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(addressLocation, 16));

                                BitmapDescriptor descriptor = BitmapDescriptorFactory.fromBitmap(resizeMapIcons("house", 80, 80));
                                MarkerOptions options = new MarkerOptions().position(addressLocation).title(strAddress).icon(descriptor);
                                mMap.addMarker(options);
                            });

                        } else {
                            Log.d(TAG, "No se encontro con Geocoding api tampoco");
                        }
                    } catch (ApiException | InterruptedException | IOException e) {
                        e.printStackTrace();
                    }
                }).start();*/
            } else {
                placeMarkerToAddress(strAddress, address);
            }
        } catch (IOException e) {
            Log.e(TAG, "Error al obtener coordenadas en base a direccion: " + e.getMessage());
        }
    }

    private void placeMarkerToAddress(String strAddress, List<Address> address) {
        LatLng p1;
        Address location=address.get(0);
        location.getLatitude();
        location.getLongitude();

        p1 = new LatLng(location.getLatitude(), location.getLongitude());
        Log.d(TAG, "Lat long found: " + p1);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(p1, 16));

        BitmapDescriptor descriptor = BitmapDescriptorFactory.fromBitmap(resizeMapIcons("house", 80, 80));
        MarkerOptions options = new MarkerOptions().position(p1).title(strAddress).icon(descriptor);
        mMap.addMarker(options);
    }

    public Bitmap resizeMapIcons(String iconName, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),getResources().getIdentifier(iconName, "drawable", getPackageName()));
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }
}
