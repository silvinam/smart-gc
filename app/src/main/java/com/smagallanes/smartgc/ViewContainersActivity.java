package com.smagallanes.smartgc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class ViewContainersActivity extends BaseActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_containers);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        Button resetMap = findViewById(R.id.reset_map);
        resetMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LatLng cordoba = new LatLng(-31.417, -64.19);
                if (mMap != null) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cordoba, 12));
                }
            }
        });
        setTitle(getString(R.string.ver_contenedores));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng recoleccionResiduos = new LatLng(-31.41623600000001, -64.191635);
        mMap.addMarker(new MarkerOptions().position(recoleccionResiduos).title("Contenedor: Recolección Residuos")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));

        LatLng cpcArguello = new LatLng(-31.341857999999952, -64.25368000000002);
        mMap.addMarker(new MarkerOptions().position(cpcArguello).title("Contenedor: CPC Argüello")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

        LatLng cpcRuta20 = new LatLng(-31.432902000000002, -64.244555);
        mMap.addMarker(new MarkerOptions().position(cpcRuta20).title("Contenedor: CPC Ruta 20")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

        LatLng cpcVillaLibertadorRancagua = new LatLng(-31.467069000000013, -64.22514100000001);
        mMap.addMarker(new MarkerOptions().position(cpcVillaLibertadorRancagua).title("Contenedor: CPC Villa El Libertador - Rancagua")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

        LatLng cpcRancagua = new LatLng(-31.370054000000007, -64.14839700000002);
        mMap.addMarker(new MarkerOptions().position(cpcRancagua).title("Contenedor: CPC Rancagua")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

        // Add a marker in Sydney and move the camera
        LatLng cordoba = new LatLng(-31.417, -64.19);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cordoba, 12));
    }
}