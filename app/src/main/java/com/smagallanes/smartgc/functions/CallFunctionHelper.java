package com.smagallanes.smartgc.functions;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;
import com.smagallanes.smartgc.model.SpecialService;

import java.util.HashMap;
import java.util.Map;

public class CallFunctionHelper {
    private static final String TAG = CallFunctionHelper.class.getSimpleName();
    private static FirebaseFunctions sFunctions = FirebaseFunctions.getInstance();

    public static Task<String> requestSpecialServiceRequest(final String token) {
        Log.i(TAG, "Calling requestSpecialServiceRequest");
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference().child("special-services");
        // Register special service request in DB
        SpecialService specialService = new SpecialService(token);
        databaseReference.push().setValue(specialService);
        return sFunctions
                .getHttpsCallable("scheduleSpecialService")
                .call(/*data*/)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        String result = (String) task.getResult().getData();
                        Log.i(TAG, "Result from request: " + result);
                        return result;
                    }
                });
    }

    public static Task<String> requestSpecialServiceRequestWithDate(final String token, int year,
                    int month, int day, int hour, int minute, int second) {
        Log.i(TAG, "Calling requestSpecialServiceRequestWithDate with year: " + year + " month: " + month + " day: " + day + " hour: " + hour + " minute: " + minute + " second: " + second);
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference().child("special-services");
        // Register special service request in DB
        SpecialService specialService = new SpecialService(token);
        databaseReference.push().setValue(specialService);
        Map<String, Integer> data = new HashMap<>();
        data.put("year", year);
        data.put("month", month);
        data.put("day", day);
        data.put("hour", hour);
        data.put("minute", minute);
        data.put("second", second);
        return sFunctions
                .getHttpsCallable("scheduleSpecialServiceWithDate")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        String result = (String) task.getResult().getData();
                        Log.i(TAG, "Result from request: " + result);
                        return result;
                    }
                });
    }
}
