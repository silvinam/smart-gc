package com.smagallanes.smartgc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class ConstructionWasteExplanationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_construction_waste_explanation);
        setTitle(getString(R.string.residuos_obras));

    }
}