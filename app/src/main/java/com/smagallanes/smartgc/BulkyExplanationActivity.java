package com.smagallanes.smartgc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class BulkyExplanationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulky_explanation);
        setTitle(getString(R.string.restos_voluminosos));
    }
}