package com.smagallanes.smartgc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class RateUsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_us);
        setTitle(getString(R.string.calificanos));
    }

    public void sendRating(View view) {
        //Toast.makeText(RateUsActivity.this, R.string.rate_sent, Toast.LENGTH_LONG).show();
        Snackbar.make(findViewById(android.R.id.content), R.string.rate_sent, Snackbar.LENGTH_SHORT)
               /* .setAction("CLOSE", view1 -> {

                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))*/
                .show();
        Handler handler = new Handler(getMainLooper());
        handler.postDelayed(this::finish, 3000);
    }
}