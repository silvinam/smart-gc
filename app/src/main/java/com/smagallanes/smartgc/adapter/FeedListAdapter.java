package com.smagallanes.smartgc.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smagallanes.smartgc.R;
import com.smagallanes.smartgc.model.FeedItem;

import java.util.List;

public class FeedListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<FeedItem> feedItems;

    public FeedListAdapter(Activity activity, List<FeedItem> feedItems) {
        this.activity = activity;
        this.feedItems = feedItems;
    }

    @Override
    public int getCount() {
        return feedItems.size();
    }

    @Override
    public Object getItem(int location) {
        return feedItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.feed_item, null);
        }

        TextView name = convertView.findViewById(R.id.name);
        TextView timestamp = convertView.findViewById(R.id.when);
        TextView statusMsg = convertView.findViewById(R.id.status);
        TextView affectedZones = convertView.findViewById(R.id.affected_zones);
        ImageView icon = convertView.findViewById(R.id.icon);
        ImageView feedImageView = convertView.findViewById(R.id.news_image);

        FeedItem item = feedItems.get(position);

        name.setText(item.getName());

        timestamp.setText(item.getWhen());

        // Chcek for empty status message
        if (!TextUtils.isEmpty(item.getStatus())) {
            statusMsg.setText(item.getStatus());
            statusMsg.setVisibility(View.VISIBLE);
        } else {
            // status is empty, remove from view
            statusMsg.setVisibility(View.GONE);
        }
        affectedZones.setVisibility(View.VISIBLE);
        affectedZones.setText(item.getAffectedZones());

        // user profile pic
        icon.setImageDrawable(activity.getDrawable(item.getIconDrawableId()));

        // Feed image
        feedImageView.setImageDrawable(activity.getDrawable(item.getNewsImageId()));
        feedImageView.setVisibility(View.VISIBLE);

        return convertView;
    }

}
