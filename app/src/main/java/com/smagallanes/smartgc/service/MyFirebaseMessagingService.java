package com.smagallanes.smartgc.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.smagallanes.smartgc.BaseActivity;
import com.smagallanes.smartgc.MainMenuActivity;
import com.smagallanes.smartgc.R;

import java.util.HashSet;

import static android.content.ContentValues.TAG;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private final static String CHANNEL_ID = "Main_channel";
    private NotificationCompat.Builder mBuilder;

    @Override
    public void onCreate() {
        super.onCreate();
        initNotificationChannel();
        initNotificationBuilder();
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        String notificationTitle = null;
        String notificationBody = null;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            notificationTitle = remoteMessage.getNotification().getTitle();
            notificationBody = remoteMessage.getNotification().getBody();
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        //sendNotification(notificationTitle, notificationBody);
        if (BaseActivity.areNotificationsEnabled()) {
            sendNotificationToChannel(notificationTitle, notificationBody);
        }
    }

    private void sendNotificationToChannel(String notificationTitle, String notificationBody) {
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(getApplicationContext());
        Log.d(TAG, "Mostrando notificacion");

        mBuilder.setContentText(notificationTitle) //"El camion de basura especial pasará pronto por su domicilio."
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(notificationBody))
                .setSmallIcon(R.drawable.ic_launcher_round)
                        //.bigText("El camion de basura especial pronto pasará por su domicilio. Controle que la basura esté afuera."))
                .setTicker("Aca va algo...");

        notificationManager.notify(0, mBuilder.build());

        SharedPreferences sharedPref = getSharedPreferences("SMARTGC", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putStringSet("SCHEDULED_SERVICES", new HashSet<>());
        editor.apply();
    }

    private void initNotificationBuilder() {
        mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Nueva notificación de SMART GC")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
    }

    private void initNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void sendRegistrationToServer(String token) {
    }
}
