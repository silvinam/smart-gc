package com.smagallanes.smartgc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class WetExplanationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wet_explanation);
        setTitle(getString(R.string.humedos));
    }
}