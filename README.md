# README #

### Sistema inteligente para la manipulación de residuos urbanos ###
## Aplicación móvil Smart GC ##

La aplicación móvil Smart GC  fue desarrollada para dar soporte a la gestión de residuos en la ciudad de Córdoba en el marco de Ciudades Inteligentes.

En el siguiente enlace se encuentra una breve presentación con los lineamientos generales que motivaron su desarrollo y las funcionalidades principales que la componen:

[Aplicación Móvil Smart GC - Demo](https://docs.google.com/presentation/d/170zhlpAE0qhwyXMyjaYtod1wIBJ-s3Y4ehUkaOGVvmM/edit?usp=sharing)

### ¿Para qué sirve este repositorio? ###

Este repositorio contiene el codigo fuente para la aplicación móvil Smart GC.

### ¿Cómo me configuro? ###

Android Studio es el Entorno de Desarrollo Integrado(IDE) utilizado para el desarrollo de la presente aplicación.

Se plantea una solución full Google dado que ofrece infraestructura mundial, segura y fiable, a lo que se suma la compatibilidad óptima que ofrece, su seguridad y la escalabilidad, siempre enfocados en una alternativa que vaya de la mano con el concepto de Smart Cities y la migración de todos los servicios a la nube.

Para el backend se emplea Firebase y Google Cloud.  El uso de estas herramientas permite sincronización en tiempo real y registro de eventos del usuario mediante Firebase. Los Servlets de Java que se ejecutan en el entorno flexible de Google Cloud Platform (GCP) están a la escucha de registros de usuarios nuevos almacenados en Firebase y los procesan.


Para poder probar la aplicación solo es necesario descargar el archivo _apk_ e instalarlo en tu dispositivo Android. El archivo _apk_ esta ubicado dentro de la carpeta "Demo Prototipo Tecnologico".


### Testing de Integración ###

TODO